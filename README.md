# Simple account policy

This module implements a simple account policy with the following
configurable rules:
- Username email and username must match (enforces an email as username)
- Username allowed patterns (usernames must follow this pattern to be
  valid)
- Username ignore patterns (don't apply policy for usernames matching
  this pattern)
- Email allowed patterns (email must follow this pattern to be valid)
- Cron check interval (interval at which module will check user policy
  on all users)
- The inactive period. When users don't login for this period of time,
  they will be blocked.
- Inactive warning period. If users are about to be blocked, send out
  a warning mail.
- The warning mail message and subject.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/simple_account_policy).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/simple_account_policy).


## Requirements

This does not require any other module.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

**General usage**

After installing the module is configured with these default rules:
- username_match_email: 1
- username_match_patterns: { } (none)
- username_ignore_patterns: { } (none)
- email_match_patterns: { } (none)
- inactive_interval: 86400 (1 day)
- inactive_period: '3 months'
- inactive_warning: '3 weeks'

Configuration is found under the "`People`" configuration item.
`/admin/config/people/account_policy`


## Maintainers

- Mschudders - [Mschudders](https://www.drupal.org/u/mschudders)
- Kris Booghmans - [kriboogh](https://www.drupal.org/u/kriboogh)

**This project has been sponsored by:**
- [Calibrate](https://www.calibrate.be)
  In the past fifteen years, we have gained large expertise in
  consulting organizations in various industries.
