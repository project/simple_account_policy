<?php

namespace Drupal\simple_account_policy;

use Drupal\Core\Config\Config;
use Drupal\user\UserInterface;

/**
 * Implements interface.
 */
interface AccountPolicyInterface {

  /**
   * Decide if the policy should be applied for this user or not.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return bool
   *   Return statement
   */
  public function applyPolicy(UserInterface $user): bool;

  /**
   * Validate an email string for a given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   * @param array $data
   *   An array containing the data to validate for the given user.
   *   Should contain a 'mail' and 'name' key.
   *
   * @return array
   *   An array of policy violations (errors) for 'mail' and 'name'.
   */
  public function validate(UserInterface $user, array $data): array;

  /**
   * Return the policy that applies for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return array
   *   Array containing a string describing the policy for 'mail' and 'name'.
   */
  public function policy(UserInterface $user): array;

  /**
   * Get the timestamp an inactive user will get blocked.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return int
   *   Return statement
   */
  public function getBlockTime(UserInterface $user): int;

  /**
   * Get the timestamp to warn an inactive user will get blocked.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return int
   *   Return statement
   */
  public function getWarningTime(UserInterface $user): int;

  /**
   * Get the timestamp when a user can be removed.
   *
   * @return int
   *   The timestamp after which a user is deleted.
   */
  public function getDeleteAfterTime(): int;

  /**
   * The interval in seconds to check for inactive users.
   *
   * @return int
   *   Return statement
   */
  public function inactiveInterval(): int;

  /**
   * Decide if we need to issue a warning or not.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return bool
   *   Return statement
   */
  public function shouldIssueWarning(UserInterface $user): bool;

  /**
   * Decide if we need to delete a user or not.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user that needs to be checked.
   *
   * @return bool
   *   True is the user needs to be deleted.
   */
  public function shouldBeDeleted(UserInterface $user): bool;

  /**
   * Check if a user already got a warning.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return bool
   *   Return statement
   */
  public function warningIssued(UserInterface $user): bool;

  /**
   * Send out a warning event.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return bool
   *   Return statement
   */
  public function issueWarning(UserInterface $user): void;

  /**
   * Decide if user is considered active or not.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   */
  public function isInactive(UserInterface $user): bool;

  /**
   * Block a given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return void
   *   Return statement
   */
  public function block(UserInterface $user): void;

  /**
   * Delete a given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   */
  public function delete(UserInterface $user): void;

  /**
   * Make a given user active again.
   *
   * The user will be reactivated.
   * And their last login date will be reset to today
   * to prevent them being blocked again by cron.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to activate.
   *
   * @return void
   *   Return statement
   */
  public function activate(UserInterface $user): void;

  /**
   * Get the policy configuration.
   *
   * @return \Drupal\Core\Config\Config
   *   The policy configuration object.
   */
  public function getConfiguration(): Config;

}
