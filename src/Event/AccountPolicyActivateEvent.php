<?php

namespace Drupal\simple_account_policy\Event;

use Drupal\simple_account_policy\AccountPolicyInterface;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a users account is activated.
 */
class AccountPolicyActivateEvent extends Event {

  const EVENT_NAME = 'simple_account_policy_activate';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * The account policy.
   *
   * @var \Drupal\simple_account_policy\AccountPolicyInterface
   */
  public $policy;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The activated user account.
   * @param \Drupal\simple_account_policy\AccountPolicyInterface $policy
   *   The account policy that triggered the event.
   */
  public function __construct(UserInterface $account, AccountPolicyInterface $policy) {
    $this->account = $account;
    $this->policy = $policy;
  }

}
