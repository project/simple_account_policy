<?php

namespace Drupal\simple_account_policy\Event;

use Drupal\simple_account_policy\AccountPolicyInterface;
use Drupal\user\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that is fired when a users account is going to be deleted.
 */
class AccountPolicyDeleteEvent extends Event {

  const EVENT_NAME = 'simple_account_policy_delete';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * The account policy.
   *
   * @var \Drupal\simple_account_policy\AccountPolicyInterface
   */
  public $policy;

  /**
   * The cancel method.
   *
   * @var string
   *
   * @see \user_cancel_methods()
   */
  private $cancelMethod;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account of the user whos account is being deleted.
   * @param \Drupal\simple_account_policy\AccountPolicyInterface $policy
   *   The account policy that triggered the event.
   * @param string $cancelMethod
   *   Cancel method string.
   */
  public function __construct(
    UserInterface $account,
    AccountPolicyInterface $policy,
    string $cancelMethod,
  ) {
    $this->account = $account;
    $this->policy = $policy;
    $this->cancelMethod = $cancelMethod;
  }

  /**
   * Get the cancel method for this delete event.
   *
   * @return string
   *   The cancel method string.
   */
  public function getCancelMethod(): string {
    return $this->cancelMethod;
  }

}
