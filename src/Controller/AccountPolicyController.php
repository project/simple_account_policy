<?php

namespace Drupal\simple_account_policy\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\simple_account_policy\AccountPolicyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Account Policy Controller.
 */
class AccountPolicyController extends ControllerBase {

  /**
   * The account policy.
   *
   * @var \Drupal\simple_account_policy\AccountPolicyInterface
   */
  protected $accountPolicy;

  /**
   * The redirect destination interface.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * ModalFormContactController constructor.
   *
   * @param \Drupal\simple_account_policy\AccountPolicyInterface $account_policy
   *   The account policy.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination.
   */
  public function __construct(AccountPolicyInterface $account_policy, RedirectDestinationInterface $redirect_destination) {
    $this->accountPolicy = $account_policy;
    $this->redirectDestination = $redirect_destination;

  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_account_policy'),
      $container->get('redirect.destination')
    );
  }

  /**
   * Activate the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return statement.
   */
  public function activate(UserInterface $user) {

    $this->accountPolicy->activate($user);
    return new RedirectResponse($this->redirectDestination->get());
  }

  /**
   * Block the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   User.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Return statement.
   */
  public function block(UserInterface $user) {

    $this->accountPolicy->block($user);
    return new RedirectResponse($this->redirectDestination->get());
  }

}
