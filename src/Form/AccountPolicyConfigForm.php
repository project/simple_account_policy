<?php

namespace Drupal\simple_account_policy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements custom config form to configure the account policy.
 */
class AccountPolicyConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'account_policy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'simple_account_policy.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('simple_account_policy.settings');

    $form['#title'] = $this->t('Account policy settings');

    $form['policy_rules'] = [
      '#type' => 'details',
      '#title' => $this->t('Policy rules'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['policy_rules']['username_prevent_changes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent username changes'),
      '#description' => $this->t("Check this box if it's not allowed to change the username."),
      '#default_value' => $config->get('username_prevent_changes') ?? 0,
    ];

    $form['policy_rules']['username_match_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Username match email'),
      '#description' => $this->t("Check this box if the username must match the email address."),
      '#default_value' => $config->get('username_match_email') ?? 1,
    ];

    $form['policy_rules']['username_match_patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Username match patterns'),
      '#description' => $this->t("Add a pattern on each line to which the username needs to comply. You can use regex notation."),
      '#default_value' => implode("\n", $config->get('username_match_patterns') ?? []),
    ];

    $form['policy_rules']['email_match_patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email match patterns'),
      '#description' => $this->t("Add a pattern on each line to which the email needs to comply. You can use regex notation."),
      '#default_value' => implode("\n", $config->get('email_match_patterns') ?? []),
    ];

    $form['policy_bypass'] = [
      '#type' => 'details',
      '#title' => $this->t('Policy bypass'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['policy_bypass']['username_ignore_patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Username ignore patterns'),
      '#description' => $this->t("Add a username pattern on each line to exclude specific users from the policy by their username. You can use regex notation. You can also exlcude users from the policy based on roles by giving a role the 'bypass account policy' permission."),
      '#default_value' => implode("\n", $config->get('username_ignore_patterns') ?? []),
    ];

    $form['delete'] = [
      '#type' => 'details',
      '#title' => $this->t('Delete user settings'),
      '#description' => $this->t('Set timings on when users must be automatically deleted.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['delete']['delete_after_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Delete user after given time'),
      '#description' => $this->t('The time that must have elapsed before a user should be deleted. Leave empty to not delete a user. You can use strtotime notation e.g. "1 year"'),
      '#default_value' => $config->get('delete_after_time') ?? '1 year',
      '#maxlength' => 180,
    ];
    $form['delete']['user_cancel_method'] = [
      '#type' => 'select',
      '#options' => user_cancel_methods()['#options'],
      '#title' => $this->t('User cancel method'),
      '#description' => $this->t('Select a user cancellation method.'),
      '#default_value' => $config->get('user_cancel_method') ?? 'user_cancel_reassign',
    ];

    $form['inactive_scheduling'] = [
      '#type' => 'details',
      '#title' => $this->t('Scheduling'),
      '#description' => $this->t('Set timings on when users must be automatically blocked after long inactivity.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['inactive_scheduling']['inactive_interval'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Inactive interval'),
      '#description' => $this->t("The cron interval in seconds for checking if users need to be put inactive. Set to 0 to disable auto blocking of users via cron."),
      '#default_value' => $config->get('inactive_interval') ?? 86400,
      '#maxlength' => 180,
    ];

    $form['inactive_scheduling']['inactive_period'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Inactive period'),
      '#description' => $this->t('The time from the last login date for a user to be considered inactive. You can use strtotime notation e.g. "3 months"'),
      '#default_value' => $config->get('inactive_period') ?? '3 months',
      '#maxlength' => 180,
    ];

    $form['inactive_scheduling']['inactive_warning'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Inactive warning'),
      '#description' => $this->t('The warning period till the user will be put inactive. Leave empty to not send a message. You can use strtotime notation e.g. "3 weeks"'),
      '#default_value' => $config->get('inactive_warning') ?? '3 weeks',
      '#maxlength' => 180,
    ];

    $form['inactive_warning_mail'] = [
      '#type' => 'details',
      '#title' => $this->t('Warning email'),
      '#description' => $this->t('Email send out when the inactive warning period has expired.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['inactive_warning_mail']['inactive_warning_mail_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $config->get('inactive_warning_mail.subject'),
      '#maxlength' => 180,
    ];
    $form['inactive_warning_mail']['inactive_warning_mail_body'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Body'),
      '#default_value' => $config->get('inactive_warning_mail.body'),
      '#rows' => 8,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('simple_account_policy.settings')
      ->set('username_prevent_changes', $form_state->getValue('username_prevent_changes'))
      ->set('username_match_email', $form_state->getValue('username_match_email'))
      ->set('username_match_patterns', explode('\n', $form_state->getValue('username_match_patterns')))
      ->set('username_ignore_patterns', explode('\n', $form_state->getValue('username_ignore_patterns')))
      ->set('email_match_patterns', explode('\n', $form_state->getValue('email_match_patterns')))
      ->set('inactive_interval', $form_state->getValue('inactive_interval'))
      ->set('inactive_period', $form_state->getValue('inactive_period'))
      ->set('inactive_warning', $form_state->getValue('inactive_warning'))
      ->set('inactive_warning_mail.body', $form_state->getValue('inactive_warning_mail_body'))
      ->set('inactive_warning_mail.subject', $form_state->getValue('inactive_warning_mail_subject'))
      ->set('delete_after_time', $form_state->getValue('delete_after_time'))
      ->set('user_cancel_method', $form_state->getValue('user_cancel_method'))
      ->save();
  }

}
