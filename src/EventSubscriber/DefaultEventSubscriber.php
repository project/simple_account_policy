<?php

namespace Drupal\simple_account_policy\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\simple_account_policy\Event\AccountPolicyActivateEvent;
use Drupal\simple_account_policy\Event\AccountPolicyBlockEvent;
use Drupal\simple_account_policy\Event\AccountPolicyDeleteEvent;
use Drupal\simple_account_policy\Event\AccountPolicyWarningEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Default account policy events handler.
 */
class DefaultEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * DefaultEventSubscriber constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(
    MessengerInterface $messenger,
    LoggerChannelFactoryInterface $loggerFactory,
    ConfigFactoryInterface $config_factory,
    MailManagerInterface $mail_manager,
  ) {
    $this->messenger = $messenger;
    $this->loggerFactory = $loggerFactory->get('simple_account_policy');
    $this->configFactory = $config_factory;
    $this->mailManager = $mail_manager;
  }

  /**
   * Event callback to AccountPolicyActivateEvent.
   *
   * @param \Drupal\simple_account_policy\Event\AccountPolicyActivateEvent $event
   *   Event.
   */
  public function accountPolicyActivateEvent(AccountPolicyActivateEvent $event) {

    // Also un-flood the user.
    if (Database::getConnection()->schema()->tableExists('flood')) {
      Database::getConnection()->delete('flood')
        ->condition('event', 'user.failed_login_user')
        ->condition('identifier', $event->account->id() . '-%', 'LIKE')
        ->execute();
    }

    // Make sure the account is active again,
    // reset the last login date to now,
    // so it wont be blocked again.
    $event->account->activate();
    $event->account->setLastAccessTime(time());
    $event->account->save();
  }

  /**
   * Event callback to AccountPolicyBlockEvent.
   *
   * @param \Drupal\simple_account_policy\Event\AccountPolicyBlockEvent $event
   *   Event.
   */
  public function accountPolicyBlockEvent(AccountPolicyBlockEvent $event) {
    $event->account->block();
    $event->account->save();
  }

  /**
   * Event callback to AccountPolicyDeleteEvent.
   *
   * @param \Drupal\simple_account_policy\Event\AccountPolicyDeleteEvent $event
   *   The delete event.
   */
  public function accountPolicyDeleteEvent(AccountPolicyDeleteEvent $event) {
    try {
      $this->loggerFactory->notice($this->t('User @name deleted', [
        '@name' => $event->account->label(),
      ]));

      user_cancel([
        'user_cancel_notify' => 0,
        'user_cancel_method' => $event->getCancelMethod(),
        'user_cancel_confirm' => 0,
        'uid' => $event->account->id(),
      ], $event->account->id(), $event->getCancelMethod());
      $batch =& batch_get();
      $batch['progressive'] = FALSE;
      if (PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')) {
        return drush_backend_batch_process();
      }
      else {
        return batch_process();
      }
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($this->t('User deletion FAIL @name', [
        '@name' => $event->account->label(),
      ]));
    }
  }

  /**
   * Event callback to AccountPolicyWarningEvent.
   *
   * @param \Drupal\simple_account_policy\Event\AccountPolicyWarningEvent $event
   *   Event.
   */
  public function accountPolicyWarningEvent(AccountPolicyWarningEvent $event) {

    $account = $event->account;

    $params['account'] = $account;
    $langcode = $account->getPreferredLangcode();

    // Get the custom site notification email to use as the from email address
    // if it has been set.
    $site_mail_config = $this->configFactory->getEditable('system.site');
    $site_mail = $site_mail_config->get('mail_notification');
    // If the custom site notification email has not been set, we use the site
    // default for this.
    if (empty($site_mail)) {
      $site_mail = $site_mail_config->get('mail');
    }
    if (empty($site_mail)) {
      $site_mail = ini_get('sendmail_from');
    }

    // Get the from email if set in config.
    $config = $this->configFactory->getEditable('simple_account_policy.settings');
    if (!empty($config->get('inactive_warning_mail.from'))) {
      $site_mail = $config->get('inactive_warning_mail.from');
    }

    /** @var \Drupal\Core\Mail\MailManagerInterface $mail */
    $this->mailManager->mail('simple_account_policy', 'inactive_warning_mail', $account->getEmail(), $langcode, $params, $site_mail);

    // The actual message is handled in @see simple_account_policy_mail.
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[AccountPolicyActivateEvent::EVENT_NAME][] = ['accountPolicyActivateEvent'];
    $events[AccountPolicyBlockEvent::EVENT_NAME][] = ['accountPolicyBlockEvent'];
    $events[AccountPolicyWarningEvent::EVENT_NAME][] = ['accountPolicyWarningEvent'];
    $events[AccountPolicyDeleteEvent::EVENT_NAME][] = ['accountPolicyDeleteEvent'];
    return $events;
  }

}
