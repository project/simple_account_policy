<?php

/**
 * @file
 * Tokens.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function simple_account_policy_token_info() {
  $types['account_policy'] = [
    'name' => t('Account policy'),
    'description' => t('Tokens related to the account policy.'),
    'needs-data' => 'user',
  ];

  $accountPolicy['block_period'] = [
    'name' => t('Block period'),
    'description' => t("The period after which the user will be blocked."),
  ];

  return [
    'types' => $types,
    'tokens' => ['account_policy' => $accountPolicy],
  ];
}

/**
 * Implements hook_tokens().
 */
function simple_account_policy_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $replacements = [];

  if ($type == 'account_policy') {
    /** @var \Drupal\simple_account_policy\AccountPolicyInterface $accountPolicy */
    $accountPolicy = \Drupal::service('simple_account_policy');

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'block_period':
          /** @var \Drupal\user\UserInterface $user */
          $user = $data['user'];
          $block_time = $accountPolicy->getBlockTime($user);
          $langcode = $user->getPreferredLangcode();
          $replacements[$original] = simple_account_policy_time_ago($block_time, "@output", "", $langcode);
          break;
      }
    }
  }

  return $replacements;
}
